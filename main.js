//Creating the canvas and the context
const canvas = document.getElementById('canvas1');
const context = canvas.getContext('2d');

//Declarations
//Creating images for the game
var backgroundImg1 = new Image();
var backgroundImg2 = new Image();
backgroundImg1.src = 'assets/background.jpg';
backgroundImg2.src = 'assets/background.jpg';

var floorImg1 = new Image();
var floorImg2 = new Image();
floorImg1.src = 'assets/floor.png';
floorImg2.src = 'assets/floor.png';

var playerImg = new Image();
playerImg.src = 'assets/Mplayer.png';

var smallLogImg = new Image();
var bigLogImg = new Image();
smallLogImg.src = 'assets/smallLog.png';
bigLogImg.src = 'assets/bigLog.png';


var bigfootImg = new Image();
bigfootImg.src = 'assets/bigfootHands.png'

var smallLogNum = 0;
var smallLogBreaking = false;
var bigLogNum = 0;
var bigLogBreaking = false;

//Position variables
var backgroundImg1_xPos = 0;
var backgroundImg2_xPos = 1920;

var floorImg1_xPos = 0;
var floorImg2_xPos = 1920;
var floorImgs_yPos = 935;

var playerY_Pos = 690;

var smallLog_xPos = Math.floor(Math.random() * 2000) + 2000;
var smallLog_yPos = 910;
var bigLog_xPos = Math.floor(Math.random() * 2000) + 2000 + Math.random() * 1000;
var bigLog_yPos = 910;
var distanceOfLogs = 0;
var randomRespawnPos = 0;

var bigfoot_yPos = -50;
var bigfoot_xPos = -20;
var nextPos = 0;

//Player Animation variables
var playerRunTimer = 1;
var jumping = false;
var buttonJumping = false;
var falling = false;

//Speed variables
var backgroundSpeed = 2;
var floorSpeed = 15;

//Score variables
var score = 0;
var scoreIncrease = 1;
/////////////////////////////////////////////////////////////////////////////////////////////////////
var highScore = 0;
highScore = localStorage.getItem("highscoreKey");

//Gamestate variables
var alive = true;
var lives = 3;

//Collision variables
var smallCanCollide = true;
var bigCanCollide = true;

//Audio variables
var stompSound = new Audio('assets/stomp.ogg');
var stompSound1 = new Audio('assets/stomp.ogg');
var nextSound = 0;
var smallLogSound = new Audio('assets/smallLog.ogg');
var bigLogSound = new Audio('assets/bigLog.ogg');


//Jump button
var jumpButton = document.getElementById("jumpButton");

function createJumpButton(context, func) 
{
    jumpButton.type = "button";
    jumpButton.value = "Jump Button";
    jumpButton.onclick = func;
    context.appendChild(jumpButton);
}

//Respawn button
var respawnButton = document.getElementById("respawnButton");
var buttonXPos = respawnButton.style.left;//on screen pos
var buttonXPos2 = -respawnButton.style.left + "10000px";//Off screen pos
var reset = false;//If true the respawn button appears, if false disapears

function createRespawnButton(context, func)
{
    respawnButton.type = "button";
    respawnButton.value = "Respawn Button";
    respawnButton.onclick = func;
    context.appendChild(respawnButton);
}

//Mute button
var muteButton = document.getElementById("muteButton");
var muted = false;

function createMuteButton(context, func) 
{
    muteButton.type = "button";
    muteButton.value = "Mute Button";
    muteButton.onclick = func;
    context.appendChild(muteButton);
}

//character button
var male = 1;
var female = 2;
var gender = male;
var genderButton = document.getElementById("genderButton");

function createGenderButton(context, func) 
{
    genderButton.type = "button";
    genderButton.value = "Character Button";
    genderButton.onclick = func;
    context.appendChild(genderButton);
}

//When window loads make buttons
window.onload = function() 
{
    //Creates jump button
    createJumpButton(document.body, function() {

        if (jumping != true && falling != true) jumping = true;
        jumpButton.blur();
    });
    
    //Creates respawn button
    createRespawnButton(document.body, function() {

        if (alive == false) reset = true;
        respawnButton.blur();
    });

    //Creates mute button
    createMuteButton(document.body, function() {
    
        if (muted == false)
        {
            muted = true;
            muteButton.style.background = "red";
        }
        else
        {
            muted = false;
            muteButton.style.background = "#89c403";
        }
        muteButton.blur();
    });

    //Creates character button
    createGenderButton(document.body, function() {
        
        //2 if statements that change what image is used for player
        if(gender == male)
        {
            gender = female;
            playerImg.src = 'assets/Fplayer.png';
            genderButton.style.background = "#ff0084";
        }
        else if(gender == female)
        {
            gender = male;
            playerImg.src = 'assets/Mplayer.png';
            genderButton.style.background = "#0044ff";
        }
        genderButton.blur(); //Removes the button from focus after its clicked (prevents keys activating button after button pressed with mouse).
    });
};





/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


gameLoop();//Begins the game update loop
setInterval(animatePlayer, 70);//Initiate Player animation
setInterval(logCollision, 90);
setInterval(moveBigfoot, 300);
//Game update loop
function gameLoop()
{
    draw();
    update();

    if (alive == true) respawnButton.style.left = buttonXPos2;
    if (alive == false) respawnButton.style.left = buttonXPos;

    window.requestAnimationFrame(gameLoop);//Loops the function
}


function update()//Main update function
{
    moveBackground();//Moves the 2 background images
    moveFloor();//Moves the 2 floor images
    moveLogs();//Moves the 2 different logs
    jump();
    scoreText();
    if (alive == false) deathScreen();

    if(alive == false)
    {
        backgroundSpeed = 0;
        floorSpeed = 0;
        scoreIncrease = 0;
    }
}

function draw()//Main draw function
{
    //Draws the background images in their correct updating positions
    context.drawImage(backgroundImg1, backgroundImg1_xPos, 0);
    context.drawImage(backgroundImg2, backgroundImg2_xPos, 0);
    //Draws the floor images in their correct updating positions (xPos's are unique, yPos's are the same)
    context.drawImage(floorImg1, floorImg1_xPos, floorImgs_yPos);
    context.drawImage(floorImg2, floorImg2_xPos, floorImgs_yPos);

    //Draws log obsticles
    context.drawImage(smallLogImg, 0, 0, 500, 500, smallLog_xPos, smallLog_yPos, 550, 400);

    //Draws the player image
    //CanvasDrawImage.drawImage(image, startX, startY, startW, startH, destinationX, DestinationY, destinationW, destinationH)
    if (alive == true)
    {
        if (jumping == false && falling == false)
        {
        if (playerRunTimer == 1) context.drawImage(playerImg, 0, 50, 225, 278, 400, 690, 300, 300);
        else if (playerRunTimer == 2) context.drawImage(playerImg, 300, 50, 225, 278, 450, 690, 300, 300);
        else if (playerRunTimer == 3) context.drawImage(playerImg, 550, 50, 225, 278, 440, 690, 300, 300);
        else if (playerRunTimer == 4) context.drawImage(playerImg, 770, 50, 225, 278, 395, 690, 300, 300);
        else if (playerRunTimer == 5) context.drawImage(playerImg, 1025, 50, 235, 278, 400, 690, 300, 300);
        else if (playerRunTimer == 6) context.drawImage(playerImg, 1325, 50, 235, 278, 460, 690, 300, 300);
        else if (playerRunTimer == 7) context.drawImage(playerImg, 1525, 50, 235, 278, 385, 690, 300, 300);
        else if (playerRunTimer == 8) context.drawImage(playerImg, 1820, 50, 235, 278, 440, 690, 300, 300);
        }
        else
        {
            if(jumping == true) context.drawImage(playerImg, 1025, 690, 240, 290, 440, playerY_Pos, 300, 300);
            else if (falling == true) context.drawImage(playerImg, 1300, 690, 240, 290, 440, playerY_Pos, 300, 300);
        }
    }
    else
    {
        if (gender == male) playerImg.src = 'assets/MplayerDead.png';
        if (gender == female) playerImg.src = 'assets/FplayerDead.png';
        context.drawImage(playerImg, 500, 935);
    }

    //Draws log obsticles
    context.drawImage(bigLogImg, 0, 0, 500, 500, bigLog_xPos, bigLog_yPos, 550, 400);

    //Draws bigfoot hands
    context.drawImage(bigfootImg, 0, 0, 1920, 1080, bigfoot_xPos, bigfoot_yPos, 1620, 1000)

}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Moves the 2 background images
function moveBackground() 
{
    //Updates the X value to move images
    backgroundImg1_xPos -= backgroundSpeed;
    backgroundImg2_xPos -= backgroundSpeed;

    //If.When the images go off the screen, reset them to start positions
    if (backgroundImg1_xPos <= -1920) backgroundImg1_xPos = 0;
    if (backgroundImg2_xPos <= 0)     backgroundImg2_xPos = 1920;
}
//Moves the 2 floor images
function moveFloor() 
{
    //Updates the X value to move images
    floorImg1_xPos -= floorSpeed;
    floorImg2_xPos -= floorSpeed;

    //If.When the images go off the screen, reset them to start positions
    if (floorImg1_xPos <= -1920) floorImg1_xPos = 0;
    if (floorImg2_xPos <= 0)     floorImg2_xPos = 1920;
}

//Moves the log obsticles
function moveLogs()
{
    //Creates random respawn coordinates for the logs to respawn to
    randomRespawnPos = Math.floor(Math.random() * 2000) + 2500;// Returns a random integer from 1 to 10:


    //If the logs spawn too close together move one further away
    distanceOfLogs = bigLog_xPos - smallLog_xPos;
    if (smallLog_xPos <= 2000 && distanceOfLogs <= 500 && distanceOfLogs >= -500) smallLog_xPos = bigLog_xPos + 2000;
    if (smallLog_xPos >= 2000 && distanceOfLogs <= 700 && distanceOfLogs >= -700) bigLog_xPos = smallLog_xPos + 2000;


    //Moves the small log
    if (smallLog_xPos <= -600) smallLog_xPos = randomRespawnPos;
    smallLog_xPos -= floorSpeed;
    //Moves the big log
    if (bigLog_xPos <= -600) bigLog_xPos = randomRespawnPos;
    bigLog_xPos -= floorSpeed;
}


//Updates the players image
function animatePlayer() 
{
    playerRunTimer++;//Change to the next frame in the running animation
    if (playerRunTimer > 8) playerRunTimer = 1;//Restart the running animation
}

function jump()
{
    //If the player presses space bar initiate the jump sequence
    document.addEventListener('keydown', function(event) 
    {
        if (event.code === 'Space')
        {
            if (falling == false) jumping = true;
        }
    });

    //Jumping of the player
    if (jumping == true)
    {
        //Moves the player up
        playerY_Pos -= 18;

        //Once the player reaches their peak jump height begin falling
        if (playerY_Pos <= 300)
        { 
            playerY_Pos = 300;
            jumping = false;
            falling = true;
        }
    }

    //falling of the player
    if (falling == true)
    {
        //Moves the player down
        playerY_Pos += 18;

        //Resets the player to begin running again once they hit the ground again
        if (playerY_Pos >= 690)
        { 
            playerY_Pos = 690;
            falling = false;
        }
    }
}

function logCollision()
{
    //Initiate small log animation if collision occurs
    if (smallLog_xPos >= 390 && smallLog_xPos <= 700 && playerY_Pos >= 640)
    {
        //Plays sound of breaking log
        if (muted == false && smallLogNum == 1) smallLogSound.play();

        if (smallLogBreaking == false) lives--;//Subtracts a life from the player
        smallLogBreaking = true;//Breaks log
        if (lives == 0) alive = false;//Ends the game
    }

    if (smallLogBreaking == true && smallCanCollide == true)
    {
        //Sets the position of the log to its breaking position
        smallLog_yPos = 840

        //Cycles to the next animation fram
        smallLogNum++;
        if(smallLogNum == 1) smallLogImg.src = 'assets/smallLogBreak1.png';
        if(smallLogNum == 2) smallLogImg.src = 'assets/smallLogBreak2.png';
        if(smallLogNum == 3) smallLogImg.src = 'assets/smallLogBreak3.png';
        if(smallLogNum == 4) smallLogImg.src = 'assets/smallLogBreak4.png';
        if(smallLogNum == 5) smallLogImg.src = 'assets/smallLogBreak5.png';
        
        if(smallLogNum >= 6 && alive == true)
        {
            smallLogBreaking = false;
            smallLogNum = 0;
            smallCanCollide = false;
        }
    }
    if(smallLog_xPos >= 2000)
    {
        smallLogImg.src = 'assets/smallLog.png'
        smallLog_yPos = 910
        smallCanCollide = true;
    }


    //Initiate big log animation if collision occurs
    if(bigLog_xPos >= 300 && bigLog_xPos <= 600 && playerY_Pos >= 640)
    {
        //Plays sound of breaking log
        if (muted == false && bigLogNum == 1) bigLogSound.play();

        if (bigLogBreaking == false) lives--;//Subtracts a life from the player
        bigLogBreaking = true;//Breaks log
        if (lives == 0) alive = false;//Ends the game
    }

    if (bigLogBreaking == true && bigCanCollide == true)
    {
        //Sets the position of the log to its breaking position
        bigLog_yPos = 885

        //Cycles to the next animation fram
        bigLogNum++;
        console.log(bigLogImg);
        if(bigLogNum == 1) bigLogImg.src = 'assets/bigLogBreak1.png';
        if(bigLogNum == 2) bigLogImg.src = 'assets/bigLogBreak2.png';
        if(bigLogNum == 3) bigLogImg.src = 'assets/bigLogBreak3.png';
        if(bigLogNum == 4) bigLogImg.src = 'assets/bigLogBreak4.png';
        
        if(bigLogNum >= 5)
        {
            bigLogBreaking = false;
            bigLogNum = 0;
            bigCanCollide = false;
        }
    }
    if(bigLog_xPos >= 2000)
    {
        bigLogImg.src = 'assets/bigLog.png'
        bigLog_yPos = 910
        bigCanCollide = true;
    }
}


function scoreText()
{

    var scoreXpos = 1900;
    var scoreYpos = 75;
    var highScoreXpos = 1895;
    var highScoreYpos = 110;

    if (alive == true)
    {
        scoreXpos = 1900;
        scoreYpos = 75;
        highScoreXpos = 1895;
        highScoreYpos = 110;
    }
    else
    {
        scoreXpos = 1060;
        scoreYpos = 550;
        highScoreXpos = 1020;
        highScoreYpos = 600;
    }

    //Updates score and high score variables
    score += scoreIncrease;
    if (highScore < score) highScore = score;

    //Score text
    context.font = "50px Comic Sans MS";
    context.fillStyle = "white";
    context.textAlign = "right";
    context.fillText("Score: " + score, scoreXpos, scoreYpos);
    //High Score text
    context.font = "25px Arial";
    context.fillStyle = "white";
    context.textAlign = "right";
    context.fillText("HighScore: " + highScore, highScoreXpos, highScoreYpos);
}


function moveBigfoot()
{
    if (alive == true)
    {
        if (nextPos == 0) 
        {
            bigfoot_yPos -= 60;
            bigfoot_xPos += 15;
            nextPos++;

            //Plays foot step sounds
            if (nextSound == 0 ) 
            {  
                if (muted == false) stompSound.play();
                nextSound = 1;
            }
            else
            {
                if (muted == false) stompSound1.play();
                nextSound = 0;
            }
        }
        else if (nextPos >= 1)
        {  
            bigfoot_yPos += 60;
            bigfoot_xPos -= 15;
            nextPos = 0;
        }
    }
}

function deathScreen()
{
    //Resets the game if the player clicks the reset button
    if (reset == true)
    {
        localStorage.setItem('highscoreKey', highScore);
        alive = true;
        lives = 4;
        score = 0;
        scoreIncrease = 1;
        if (gender == male) playerImg.src = 'assets/Mplayer.png';
        else if (gender == female) playerImg.src = 'assets/Fplayer.png';
        backgroundSpeed = 2;
        floorSpeed = 15;
        
        reset = false;
    }

}

var savedHighScore = highScore;